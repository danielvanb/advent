import re

pat = re.compile("(?P<min>\d+)-(?P<max>\d+) (?P<ltr>\w): (?P<pwd>.+)")

with open('day02.txt') as f:
  pwds = [pat.match(p).groupdict() for p in f.read().splitlines()]

print("part 1:", sum((bool)(int(p['min']) <= p['pwd'].count(p['ltr']) <= int(p['max'])) for p in pwds))

print("part 2:", sum((bool)(p['pwd'][int(p['min'])-1] == p['ltr']) ^ (p['pwd'][int(p['max'])-1] == p['ltr']) for p in pwds))