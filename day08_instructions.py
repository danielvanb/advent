import re

prg = []

with open('day08.txt') as f:
  prg = re.findall(r"(\w+) ([+-]\d+)", f.read())

def run(prg):
  done = []
  p, acc = 0, 0
  while p < len(prg) and p not in done:
    done += [p]
    ins, val = prg[p]
    p += 1
    if ins == "acc": acc += int(val)
    if ins == "jmp": p += int(val) - 1
  return p, acc

print(run(prg))

for i, (ins, val) in enumerate(prg):
  if ins == 'jmp': tmp = ('nop', val)
  elif ins == 'nop': tmp = ('jmp', val)
  else: tmp = (ins, val)

  p, acc = run(prg[:i] + [tmp] + prg[i+1:])
  if p >= len(prg):
    print(acc)