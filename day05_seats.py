import re

def getRowNr(seq):
  rows = list(range(0, 127+1))
  for s in seq:
    if s == 'F':
      rows = rows[:len(rows)//2]
    elif s == 'B':
      rows = rows[len(rows)//2:]
    else:
      print('waitwhat?')
  
  if len(rows) == 1:
    return rows[0]
  else:
    return 'not found?'

def getSeatNr(seq):
  seats = list(range(0, 7+1))
  for s in seq:
    if s == 'L':
      seats = seats[:len(seats)//2]
    elif s == 'R':
      seats = seats[len(seats)//2:]
    else:
      print('waitwhat?')
  
  if len(seats) == 1:
    return seats[0]
  else:
    return 'not found?'

def parseSeat(seat):
  gd = re.search(r'(?P<rows>[FB]+)(?P<seats>[LR]+)', seat).groupdict()
  row = getRowNr(gd['rows'])
  seat = getSeatNr(gd['seats'])
  seatid = row * 8 + seat
  #print(row, seat, seatid)
  return seatid

with open('day05.txt') as f:
  seats = f.read().splitlines()


allseats = []
hseat = 0
for seat in seats:
  st = parseSeat(seat)
  allseats += [st]
  if st > hseat:
    hseat = st

print('highest seat:', hseat)

for i in range(0, hseat+1):
  if not i in allseats and i+1 in allseats and i-1 in allseats:
    print('available:', i)

