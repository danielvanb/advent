import re

with open('day06.txt') as f:
  questions = [lines.split() for lines in re.split(r"(?:\r?\n){2,}", f.read().strip())]

counts = []
countstot = []
for group in questions:
  answ = []
  answtot = {}

  for person in group:
    for letter in person:
      if not letter in answ: answ += [letter]
      if not letter in answtot: answtot[letter] = 0
      answtot[letter] += 1

  counts += [len(answ)]  
  countstot += [sum(answtot[answ] == len(group) for answ in answtot)]

print('part one:', sum(n for n in counts))
print('part two:', sum(n for n in countstot))