def calctrees(t, step):
  lx = len(t[0])
  ly = len(t)

  x = 0
  y = 0
  nt = 0

  while y < ly:
    if t[y][x] == '#':
      nt += 1

    x += step[0]
    y += step[1]

    if x >= lx:
      x = x % (lx)

  print('trees', step[0], step[1], nt)
  return nt

with open('day03.txt') as f:
  t = [[char for char in l] for l in f.read().splitlines()]

  val = 1
  steps = [[1,1], [3,1], [5,1], [7,1], [1,2]]

  for s in steps:
    val *= calctrees(t, s)
  
  print('total:', val)