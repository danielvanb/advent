preamble = 25

with open('day09.txt') as f:
  nums = [int(l) for l in f.read().splitlines()]

def sumRange(range):
  ret = []
  for i in range:
    for j in range:
      if not (i+j) in ret:
        ret += [i+j]
  return ret

def findSumSeq(nums, number):
  j = 1
  for i in range(0, len(nums)):
    while True:
      val = sum(nums[i:j])
      if val == number:
        return nums[i:j]
      elif val > number:
        break
      j += 1

#print(nums)

for i in range(preamble, len(nums)):
  if not nums[i] in sumRange(nums[i-preamble:i]):
    print('deel 1:', nums[i])
    weakseq = findSumSeq(nums, nums[i])
    print('deel 2:', min(weakseq) + max(weakseq))
    break

