import re


def isvalid(passport):
  if not 'byr' in passport:
    return False
  if not re.match(r'^\d{4}$', passport['byr']) or int(passport['byr']) < 1920 or int(passport['byr']) > 2002:
    return False

  if not 'iyr' in passport:
    return False
  if not re.match(r'^\d{4}$', passport['iyr']) or int(passport['iyr']) < 2010 or int(passport['iyr']) > 2020:
    return False

  if not 'eyr' in passport:
    return False
  if not re.match(r'^\d{4}$', passport['eyr']) or int(passport['eyr']) < 2020 or int(passport['eyr']) > 2030:
    return False

  if not 'hgt' in passport:
    return False
  if not re.match(r'^(\d{3}cm|\d{2}in)$', passport['hgt']):
    return False
  h = re.search(r'(?P<len>\d{2,3})(?P<unit>cm|in)', passport['hgt']).groupdict()
  if h['unit'] == 'cm' and (int(h['len']) < 150 or int(h['len']) > 193):
    return False
  if h['unit'] == 'in' and (int(h['len']) < 59 or int(h['len']) > 76):
    return False

  if not 'hcl' in passport:
    return False
  if not re.match(r'^#[0-9A-Za-z]{6}$', passport['hcl']):
    return False

  if not 'ecl' in passport:
    return False
  if not passport['ecl'] in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
    return False

  if not 'pid' in passport:
    return False
  if not re.match(r'^[0-9]{9}$', passport['pid']):
    return False
  
  return True



p = []

with open('day04.txt') as f:
  for pp in [passp.split() for passp in re.split(r"(?:\r?\n){2,}", f.read().strip())]:
    ppi = {}

    for ppp in pp:
      k, v = ppp.split(':')
      ppi[k] = v
  
    p += [ppi]


valid = 0
for pp in p:
  if isvalid(pp):
    valid += 1

print('valid:', valid)
