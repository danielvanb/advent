import re

bags = {}

def broDoYouEvenBag(bro, bag):
  return any(b['type'] == bag or broDoYouEvenBag(b['type'], bag) for b in bags[bro])

def bagSubCount(bag):
  return sum(bagSubCount(b['type']) * int(b['count']) + int(b['count']) for b in bags[bag])

with open('day07.txt') as f:
  for rr in f.read().splitlines():
    bagtype, rule = re.split(r"(?P<bagtype>.+) bags contain (?P<rule>.+)\.", rr)[1:3]
    if rule != 'no other bags':
      bags[bagtype] = [re.search(r" ?(?P<count>\d+) (?P<type>.+) bags?", rl).groupdict() for rl in rule.split(',')]
    else:
      bags[bagtype] = []

findBag = 'shiny gold'

print(findBag, 'can fit in', sum(broDoYouEvenBag(bag, findBag) for bag, rule in bags.items()), 'bags')
print(findBag, 'contains', bagSubCount(findBag), 'bags')